defmodule Ninep.FileSystem do
  defstruct tree: nil, path: [], debug: false

  alias Ninep.FileSystem, as: FileSystem

  @root_id 0

  defmodule FNode do
    defstruct id: nil, name: nil, type: :file, children: nil, attributes: []

    def new_file(name) do
      %FNode{name: name, type: :file}
    end

    def new_dir(name) do
      %FNode{name: name, type: :dir, children: []}
    end
  end

  def new(debug \\ false) do
    tree = :ets.new(:ninep_filesystem, [:set, :protected])
    :ets.insert(tree, {:counter, @root_id - 1})
    fs = %FileSystem{tree: tree, path: [@root_id], debug: debug}
    root = FNode.new_dir("/")
    save_node(fs, root)
    fs
  end

  def cd(fs, path) do
    walk(fs, to_path(path))
  end

  def ls(fs) do
    fetch_children(fs)
  end

  def ls(fs, name) do
    {last, path} = to_path(name) |> List.pop_at(-1)
    path = if path == [], do: ["."], else: path
    with {:ok, fs} <- walk(fs, path),
         {:ok, nodes} <- fetch_children(fs),
         %FNode{} = node <- Enum.find(nodes, {:error, {:not_found, name}}, &(&1.name == last)) do
      case node.type do
        :file -> [node]
        :dir  ->
          with {:ok, fs} <- walk1(fs, last), do: fetch_children(fs)
      end
    end
  end

  def pwd(fs) do
    path = get_path_nodes(fs)
    {good, bad} = Enum.split_with(path, &(elem(&1, 0) == :ok))
    case bad do
      [] ->
        good = Enum.map(good, &(elem(&1, 1)))
        {:ok, good}
      _  -> {:error, bad}
    end
  end

  def create_file(fs, name, parents \\ false) do
    createN(fs, to_path(name), :file, parents)
  end

  def create_dir(fs, name, parents \\ false) do
    createN(fs, to_path(name), :dir, parents)
  end

  def copy(fs, from, to, overwrite \\ true) do
    with {:ok, from_node, _} <- get_node(fs, from),
         {:ok, to_node} <- copy_helper(fs, from_node.name, to, overwrite) do
      new_node = %FNode{from_node | id: to_node.id, name: to_node.name}
      {:ok, save_node(fs, new_node)}
    end
  end

  defp copy_helper(fs, name, to, overwrite) do
    {to_name, _} = to_path1(to)
    with {:ok, to_node, fs_to} <- get_node(fs, to) do
      if to_node.type == :file and not overwrite do
        {:error, {:already_exists, to_node}}
      else
        case to_node.type do
          :file -> {:ok, to_node}
          :dir -> create_file(fs_to, to_name <> "/" <> name)
        end
      end
    else
      {:error, {:not_found, ^to_name}} -> create_file(fs, to)
      other -> other
    end
  end

  def remove(fs, target, recursive \\ false) do
    with {:ok, node, fs} <- get_node(fs, target),
         {:ok, parent, _} <- get_node(fs) do
      if node.type == :dir and node.children != [] do
        if recursive do
          Enum.reduce_while(node.children, fn(c) ->
            
          end)
        else
          {:error, {:not_empty, target}}
        end
      else
        children = Enum.filter(parent.children, &(&1 != node.id))
        parent = %FNode{parent | children: children}
        save_node(fs, parent)
        remove_node(fs, node)
        :ok        
      end
    end
  end

  def move(fs, from, to, overwrite \\ true) do
    with {:ok, from_node, from_fs} <- get_node(fs, from),
         {:ok, from_parent, _} <- get_node(from_fs),
         {:ok, to1} <- move_helper(fs, to, overwrite) do
      case to1 do
        {name, to1} ->
          # file to dir
          with {:ok, to_dir, _} <- get_node(fs, Enum.join(to1, "/")) do
            ch = from_parent.children |> Enum.filter(&(&1 != from_node.id))
            from_parent = %FNode{from_parent | children: ch}
            save_node(fs, from_parent)
            from_node = %FNode{from_node | name: name}
            save_node(fs, from_node)
            ch = Enum.uniq([from_node.id | to_dir.children])
            to_dir = %FNode{to_dir | children: ch}
            save_node(fs, to_dir)
            {:ok, from_node}
          end
        %FNode{type: :dir} = to_node ->
          ch = from_parent.children |> Enum.filter(&(&1 != from_node.id))
          from_parent = %FNode{from_parent | children: ch}
          save_node(fs, from_parent)
          ch = [from_node.id | to_node.children]
          to_node = %FNode{to_node | children: ch}
          save_node(fs, to_node)
          {:ok, from_node}
        %FNode{type: :file} = to_node ->
          ch = from_parent.children |> Enum.filter(&(&1 != from_node.id))
          from_parent = %FNode{from_parent | children: ch}
          save_node(fs, from_parent)
          remove_node(fs, from_node)
          to_node = %FNode{from_node | id: to_node.id}
          save_node(fs, to_node)
          {:ok, to_node}
      end
    end
  end

  defp move_helper(fs, to, overwrite) do
    {to_name, to_path} = to_path1(to)
    with {:ok, to_node, _} <- get_node(fs, to) do
      if to_node.type == :file and not overwrite do
        {:error, {:already_exists, to_node}}
      else
        {:ok, to_node}
      end
    else
      {:error, {:not_found, ^to_name}} -> {:ok, {to_name, to_path}}
      other -> other
    end
  end

  # private part

  defp fetch_node(fs) do
    fetch_node(fs, hd(fs.path))
  end

  defp fetch_node(fs, id) when is_integer(id) do
    case :ets.lookup(fs.tree, id) do
      [] -> {:error, :not_found}
      [{^id, node}] -> {:ok, %FNode{node | id: id}}
    end
  end

  def fetch_children(fs) do
    fetch_children(fs, hd(fs.path))
  end

  def fetch_children(fs, id) when is_integer(id) do
    with {:ok, node} <- fetch_node(fs, id) do
      res = Enum.reduce(node.children, [], fn(c, acc) ->
        with {:ok, n} <- fetch_node(fs, c) do
          [n | acc]
        else _ -> acc
        end
      end)
      {:ok, res}
    end
  end

  defp get_node(fs) do
    with {:ok, node} <- fetch_node(fs), do: {:ok, node, fs}
  end

  defp get_node(fs, ".") do
    with {:ok, node} <- fetch_node(fs, hd(fs.path)),
         {:ok, fs} <- walk(fs, [".."]) do
      {:ok, node, fs}
    end
  end

  defp get_node(fs, "..") do
    if length(fs.path) > 1 do
      with {:ok, fs} <- walk(fs, [".."]), do: get_node(fs, ".")
    else
      get_node(fs, ".")
    end
  end

  defp get_node(fs, name) do
    {name, path} = to_path1(name)
    with {:ok, fs} <- walk(fs, path),
         {:ok, nodes} <- fetch_children(fs),
         %FNode{} = node <- Enum.find(nodes, {:error, {:not_found, name}}, &(&1.name == name)) do
      {:ok, node, fs}
    end
  end

  defp get_children(fs) do
    with {:ok, children} <- fetch_children(fs), do: {:ok, children, fs}
  end

  defp get_children(fs, name) do
    {name, path} = to_path1(name)
    with {:ok, fs} <- walk(fs, path),
         {:ok, nodes} <- fetch_children(fs), do: {:ok, nodes, fs}
  end

  defp save_node(fs, node) do
    case node.id do
      nil ->
        id = :ets.update_counter(fs.tree, :counter, {2, 1})
        true = :ets.insert_new(fs.tree, {id, node})
        %FNode{node | id: id}
      _ ->
        true = :ets.update_element(fs.tree, node.id, {2, node})
        node
    end
  end

  defp remove_node(fs, node) do
    true = :ets.delete(fs.tree, node.id)
  end

  defp create1(fs, name, type) do
    with {:ok, node} <- fetch_node(fs) do
      # check if name exists
      existing = fetch_children(fs, node.id) |>
        elem(1) |>
        Enum.find(nil, &(&1.name == name))

      if existing == nil do
        child = case type do
                  :file -> FNode.new_file(name)
                  :dir  -> FNode.new_dir(name)
                end
        child = save_node(fs, child)
        node = %FNode{node | children: [child.id | node.children]}
        save_node(fs, node)
        {:ok, child}
      else
        {:error, {:already_exists, existing}}
      end
    end
  end

  defp createN(fs, [name], type, _) do
    create1(fs, name, type)
  end

  defp createN(fs, [dir|path], type, false) do
    with {:ok, fs} <- walk1(fs, dir), do: createN(fs, path, type, false)
  end

  defp createN(fs, [dir|path], type, true) do
    with {:ok, fs} <- walk1(fs, dir) do
      createN(fs, path, type, true)
    else
      {:error, {:not_found, dir}} ->
        with {:ok, _} <- create1(fs, dir, :dir), do: createN(fs, [dir|path], type, true)
    end
  end

  def get_path_nodes(fs) do
    Enum.map(fs.path, &(fetch_node(fs, &1)))
  end

  defp walk1(fs, "") do
    {:ok, %FileSystem{fs | path: [@root_id]}}
  end

  defp walk1(fs, ".") do
    {:ok, fs}
  end

  defp walk1(fs, "..") do
    path = fs.path
    case path do
      [@root_id] -> {:ok, fs}
      _ -> {:ok, %FileSystem{fs | path: tl(fs.path)}}
    end
  end

  defp walk1(fs, name) do
    with {:ok, node} <- fetch_node(fs) do
      res = Enum.reduce_while(node.children, nil, fn(id, acc) ->
        with {:ok, n} <- fetch_node(fs, id) do
          if n.name == name, do: {:halt, {:ok, n, n.type}}, else: {:cont, acc}
        else _ -> {:cont, acc}
        end
      end)
      with {:ok, node, :dir} <- res do
        {:ok, %FileSystem{fs | path: [node.id | fs.path]}}
      else
        {:ok, _, _} -> {:error, {:not_a_dir, name}}
        nil         -> {:error, {:not_found, name}}
      end
    end
  end

  defp walk(fs, [name]) do
    walk1(fs, name)
  end

  defp walk(fs, [dir|path]) do
    with {:ok, fs} <- walk1(fs, dir), do: walk(fs, path)
  end

  defp to_path(name) do
    absolute? = String.starts_with?(name, "/")
    path = name |> String.splitter("/") |> Enum.filter(&(&1 != ""))
    if absolute?, do: ["" | path], else: path
  end

  defp to_path1(name) do
    {name, path} = to_path(name) |> List.pop_at(-1)
    path = if path == [], do: ["."], else: path
    {name, path}
  end
end
