defmodule Ninep do
  @moduledoc """
  Documentation for Ninep.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Ninep.hello
      :world

  """
  def hello do
    :world
  end
end
