defmodule NinepTest do
  use ExUnit.Case
  doctest Ninep

  alias Ninep.FileSystem, as: FS

  test "new filesystem is empty" do
    fs = FS.new()
    assert {:ok, []} = FS.ls(fs)
  end

  test "new filesystem path is root" do
    fs = FS.new()
    assert {:ok, [root]} = FS.pwd(fs)
    assert root.name == "/"
    assert root.type == :dir
    assert root.children == []
  end

  test "create file" do
    fs = FS.new()
    {:ok, file} = FS.create_file(fs, "test.txt")
    {:ok, [^file]} = FS.ls(fs)
    assert file.name == "test.txt"
    assert file.type == :file
  end

  test "create dir" do
    fs = FS.new()
    {:ok, dir} = FS.create_dir(fs, "tmp")
    {:ok, [^dir]} = FS.ls(fs)
    assert dir.name == "tmp"
    assert dir.type == :dir
    assert dir.children == []
  end

  test "change directory relative" do
    fs = FS.new()
    {:ok, dir} = FS.create_dir(fs, "tmp")
    {:ok, [root]} = FS.pwd(fs)

    assert {:ok, fs} = FS.cd(fs, "tmp")
    assert {:ok, [^dir, ^root]} = FS.pwd(fs)
    assert dir.name == "tmp"
    assert dir.type == :dir
  end

  test "change directory absolute" do
    fs = FS.new()
    {:ok, tmp} = FS.create_dir(fs, "tmp")
    {:ok, [root]} = FS.pwd(fs)

    assert {:ok, fs} = FS.cd(fs, "/tmp")
    assert {:ok, [^tmp, ^root]} = FS.pwd(fs)
    assert tmp.name == "tmp"
    assert tmp.type == :dir
  end

  test "cd to unexisting directory" do
    fs = FS.new()
    assert {:error, {:not_found, "tmp"}} = FS.cd(fs, "tmp")
  end

  test "cd to file" do
    fs = FS.new()
    {:ok, _} = FS.create_file(fs, "tmp.file")
    assert {:error, {:not_a_dir, "tmp.file"}} = FS.cd(fs, "tmp.file")
  end

  test "create sub dir" do
    fs = FS.new()
    {:ok, tmp} = FS.create_dir(fs, "tmp")
    {:ok, fs} = FS.cd(fs, "/tmp")

    {:ok, [^tmp, root]} = FS.pwd(fs)

    assert tmp.children == []
    
    {:ok, dir} = FS.create_dir(fs, "subdir")

    {:ok, [tmp, ^root]} = FS.pwd(fs)
    assert length(tmp.children) == 1
    
    {:ok, [^dir]} = FS.ls(fs)
    assert dir.name == "subdir"
    assert dir.type == :dir

    {:ok, fs} = FS.cd(fs, "subdir")

    assert {:ok, [^dir, ^tmp, ^root]} = FS.pwd(fs)
    assert tmp.name == "tmp"
    assert root.name == "/"

    {:ok, fs} = FS.cd(fs, "/")
    assert {:ok, [^tmp]} = FS.ls(fs)
  end

  test "cd sub dir" do
    fs = FS.new()
    {:ok, _} = FS.create_dir(fs, "tmp")
    {:ok, fs} = FS.cd(fs, "tmp")
    {:ok, _} = FS.create_dir(fs, "subdir1")
    {:ok, fs} = FS.cd(fs, "subdir1")
    {:ok, subdir2} = FS.create_dir(fs, "subdir2")
    {:ok, fs} = FS.cd(fs, "/")
    assert {:ok, [root]} = FS.pwd(fs)
    assert {:ok, fs} = FS.cd(fs, "/tmp/subdir1/subdir2")
    assert {:ok, [^subdir2, subdir1, tmp, ^root]} = FS.pwd(fs)
    assert subdir2.name == "subdir2"
    assert subdir1.name == "subdir1"
    assert tmp.name == "tmp"
  end

  test "create duplicate file" do
    fs = FS.new()
    {:ok, tmp} = FS.create_file(fs, "tmp")
    assert {:error, {:already_exists, ^tmp}} = FS.create_file(fs, "tmp")
  end

  test "create duplicate dir" do
    fs = FS.new()
    {:ok, tmp} = FS.create_dir(fs, "tmp")
    assert {:error, {:already_exists, ^tmp}} = FS.create_dir(fs, "tmp")
    assert {:error, {:already_exists, ^tmp}} = FS.create_dir(fs, "tmp/")
  end  

  test "cd ." do
    fs = FS.new()
    {:ok, tmp} = FS.create_dir(fs, "tmp")
    {:ok, fs} = FS.cd(fs, "tmp")
    {:ok, fs} = FS.cd(fs, ".")
    {:ok, [^tmp, _]} = FS.pwd(fs)
    assert tmp.name == "tmp"
  end

  test "cd .." do
    fs = FS.new()
    {:ok, _} = FS.create_dir(fs, "tmp")
    {:ok, fs} = FS.cd(fs, "tmp")
    {:ok, fs} = FS.cd(fs, "..")
    {:ok, [root]} = FS.pwd(fs)
    assert root.name == "/"
  end

  test "ls sub dir" do
    fs = FS.new()
    {:ok, dir2} = FS.create_dir(fs, "dir1/dir2", true)
    assert {:ok, [_root]} = FS.pwd(fs)
    assert {:ok, [^dir2]} = FS.ls(fs, "dir1")
    assert {:ok, []} = FS.ls(fs, "dir1/dir2")
  end

  test "some path walks" do
    fs = FS.new()
    assert {:ok, runfile} = FS.create_file(fs, "/sys/configs/etc/service/init/run.init", true)
    assert {:ok, stopfile} = FS.create_file(fs, "/sys/configs/etc/service/another/stop.init", true)

    assert {:ok, fs} = FS.cd(fs, "/sys/configs/etc/service")
    assert {:ok, fs} = FS.cd(fs, "../service/init")
    assert {:ok, [^runfile]} = FS.ls(fs)
    assert runfile.name == "run.init"
    assert {:ok, fs} = FS.cd(fs, "../../service/another/")
    assert {:ok, [^stopfile]} = FS.ls(fs)
    assert stopfile.name == "stop.init"
  end

  test "copy a file into another file" do
    fs = FS.new()
    {:ok, file1} = FS.create_file(fs, "test1")
    assert {:ok, file2} = FS.copy(fs, "test1", "test2")
    assert {:ok, [^file1, ^file2]} = FS.ls(fs)
  end

  test "copy a file into existing directory named" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "test1")
    {:ok, _dir1} = FS.create_dir(fs, "dir")
    assert {:ok, file2} = FS.copy(fs, "test1", "dir/test2")
    assert {:ok, [^file2]} = FS.ls(fs, "dir")
    assert file2.name == "test2"
  end

  test "copy a file into existing directory unnamed" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "test1")
    {:ok, _dir1} = FS.create_dir(fs, "dir")
    assert {:ok, file2} = FS.copy(fs, "test1", "dir")
    assert {:ok, [^file2]} = FS.ls(fs, "dir")    
  end

  test "copy a file into non-existing directory named" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "test1")
    assert {:error, {:not_found, "dir"}} = FS.copy(fs, "test1", "dir/test2")
  end

  test "copy a non-existing file" do
    fs = FS.new(true)
    assert {:error, {:not_found, "test1"}} = FS.copy(fs, "test1", "test2")
  end

  test "copy a file into existing file no overwrite" do
    fs = FS.new()
    {:ok, file1} = FS.create_file(fs, "test1")
    {:ok, file2} = FS.create_file(fs, "test2")
    assert file1.id != file2.id
    assert {:error, {:already_exists, ^file2}} = FS.copy(fs, "test1", "test2", false)
  end

  test "move a file into another file" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "file1")
    assert {:ok, file2} = FS.move(fs, "file1", "file2")
    assert file2.name == "file2"
    assert {:ok, [^file2]} = FS.ls(fs)
  end

  test "move a file into existing directory" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "file1")
    {:ok, _dir} = FS.create_dir(fs, "dir")
    assert {:ok, file2} = FS.move(fs, "file1", "dir")
    assert file2.name == "file1"
    assert {:ok, [^file2]} = FS.ls(fs, "dir")
    assert {:ok, [dir]} = FS.ls(fs)
    assert dir.name == "dir"
  end

  test "move a file into non-existing directory" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "file1")
    assert {:error, {:not_found, "dir"}} = FS.move(fs, "file1", "dir/file")
  end

  test "move a file into existing directory named" do
    fs = FS.new()
    {:ok, _file1} = FS.create_file(fs, "file1")
    {:ok, _dir} = FS.create_dir(fs, "dir")
    assert {:ok, file2} = FS.move(fs, "file1", "dir/file2")
    assert file2.name == "file2"
    assert {:ok, [^file2]} = FS.ls(fs, "dir")
    assert {:ok, [dir]} = FS.ls(fs)
    assert dir.name == "dir"
  end

  test "move a non-existing file" do
    fs = FS.new(true)
    assert {:error, {:not_found, "test1"}} = FS.move(fs, "test1", "test2")
  end

  test "move a file into existing file no overwrite" do
    fs = FS.new()
    {:ok, file1} = FS.create_file(fs, "test1")
    {:ok, file2} = FS.create_file(fs, "test2")
    assert file1.id != file2.id
    assert {:error, {:already_exists, ^file2}} = FS.move(fs, "test1", "test2", false)
  end

  test "remove existing file" do
    fs = FS.new()
    {:ok, _} = FS.create_file(fs, "test")
    :ok = FS.remove(fs, "test")
  end

  test "remove non-existing file" do
    fs = FS.new()
    {:error, {:not_found, "test"}} = FS.remove(fs, "test")
  end

end
